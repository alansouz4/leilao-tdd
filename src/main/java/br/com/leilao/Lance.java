package br.com.leilao;

public class Lance {

    public Usuario usuario;
    public Double valorDoLance;

    public Lance(Usuario usuario, Double valorDoLance) {
        this.usuario = usuario;
        this.valorDoLance = valorDoLance;
    }

    public Lance() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Double getValorDoLance() {
        return valorDoLance;
    }

    public void setValorDoLance(Double valorDoLance) {
        this.valorDoLance = valorDoLance;
    }
}
