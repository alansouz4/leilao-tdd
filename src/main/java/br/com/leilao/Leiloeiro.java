package br.com.leilao;

import java.util.List;

public class Leiloeiro {

    private String nomeDoLeiloeiro;
    private Leilao leilao;

    public Leiloeiro(String nomeDoLeiloeiro, Leilao leilao) {
        this.nomeDoLeiloeiro = nomeDoLeiloeiro;
        this.leilao = leilao;
    }

    public Leiloeiro() {
    }


    public String getNomeDoLeiloeiro() {
        return nomeDoLeiloeiro;
    }

    public void setNomeDoLeiloeiro(String nomeDoLeiloeiro) {
        this.nomeDoLeiloeiro = nomeDoLeiloeiro;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance(){
        List<Lance> lances = this.leilao.getLances();
        if(lances.isEmpty()){
            throw new RuntimeException("Não existe Lances no momento");
        }

        Lance lance = lances.get(0);

        for(Lance lanceDoFor : lances){
            if(lance.getValorDoLance() < lanceDoFor.getValorDoLance()){
                lance = lanceDoFor;
            }
        }
        return lance;
    }
}
