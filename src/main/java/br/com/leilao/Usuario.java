package br.com.leilao;

public class Usuario {

    public Integer id;
    public String nome;

    public Usuario(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Usuario() {
    }
}
