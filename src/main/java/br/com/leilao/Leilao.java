package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }
    public Leilao() {
    }


    public List<Lance> getLances() {
        return lances;
    }
    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }


    public Lance adicionarNovoLance(Lance lance) {
        Lance lanceValidado = validarLance(lance);

        this.lances.add(lanceValidado);

        return lanceValidado;
    }

    public Lance validarLance(Lance lance) {
        for (Lance lanceDoFor : this.lances){
            if(lance.getValorDoLance() < lanceDoFor.getValorDoLance()){
                throw new RuntimeException("Lance invalido");
            }
        }
        return lance;
    }


}
