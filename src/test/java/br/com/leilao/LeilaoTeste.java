package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTeste {

    private Usuario usuario;
    private Lance lance;
    private Leilao leilao;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario(1, "Alan");
        lance = new Lance(usuario, 10.00);

        List<Lance> lances = new ArrayList<>();
        lances.add(lance);

        leilao = new Leilao(lances);
    }

    @Test
    public void testarValidarLancePositivo(){
        Lance lance = new Lance(this.usuario, 20.00);

        Lance resposta = leilao.validarLance(lance);

        Assertions.assertSame(lance, resposta);
    }

    @Test
    public void testarValidarLanceNegativo(){
        Lance lance = new Lance(this.usuario, 9.00);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.validarLance(lance);});
    }

    @Test
    public void testarAdicionarNovoLancePositivo(){
        Lance lance = new Lance(this.usuario, 20.00);

        Lance retorno = leilao.adicionarNovoLance(lance);

        Assertions.assertSame(lance, retorno);
        Assertions.assertEquals(true, leilao.getLances().contains(lance));
    }

    @Test
    public void testarAdicionarNovaLanceNegativo(){
        Lance lance = new Lance(this.usuario, 9.00);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarNovoLance(lance);});
        Assertions.assertEquals(false, leilao.getLances().contains(lance));
    }
}
