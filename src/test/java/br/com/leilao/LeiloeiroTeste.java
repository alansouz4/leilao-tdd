package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTeste {

    Usuario usuario;
    Lance lance;
    Leilao leilao;
    Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario(1, "Alan");
        lance = new Lance(usuario, 20.00);
        Lance lanceMenor = new Lance(usuario, 9.00);

        List<Lance> lances = new ArrayList<>();
        lances.add(lance);
        lances.add(lanceMenor);

        leilao = new Leilao(lances);

        leiloeiro = new Leiloeiro("Augusto", leilao);
    }

    @Test
    public void testarRetornarOMaiorLance(){
        Lance lance = leiloeiro.retornarMaiorLance();

        Assertions.assertSame(this.lance, lance);
    }

    @Test
    public void testarRetornarOMaiorListaVazia(){
        List<Lance> lances = new ArrayList<>();
        leilao.setLances(lances);
        leiloeiro.setLeilao(leilao);

        Assertions.assertThrows(RuntimeException.class, () -> {leiloeiro.retornarMaiorLance();});
    }
}
